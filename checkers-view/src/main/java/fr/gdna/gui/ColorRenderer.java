package fr.gdna.gui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class ColorRenderer implements ListCellRenderer<String> {
 //ATTRIBUTS   
    private final DefaultListCellRenderer delegate;
    private final StdListModel model;
    
//CONSTRUCTEURS
    public ColorRenderer(StdListModel m) {
        delegate = new DefaultListCellRenderer();
        model = m;
    }
//REQUETES  
    
// COMMANDES   
// OUTILS

    @Override
    public Component getListCellRendererComponent(JList<? extends String> jlist, String e, int i, boolean bln, boolean bln1) {
        Component result = delegate.getListCellRendererComponent(jlist, i, i, bln, bln1);
        if (model.isDispo(e)) {
            result.setEnabled(false);
            result.setForeground(Color.blue);
        } else {
            result.setForeground(Color.red);
        }
        delegate.setText((e == null) ? "" : e.toString());
        return result;
    }
}
