package fr.gdna.gui;

import fr.gdna.model.Board;
import fr.gdna.model.Coord;
import fr.gdna.model.Piece;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class BoardPanel extends JPanel {

// ATTRIBUTS
    private final Board board;
    private Map<Coord, CasePanel> cases;  
    private final Checkers checkers;
    
    private boolean inRightSide;

// CONSTRUCTEURS
    public BoardPanel(Board b, Checkers c) {
        super(new GridLayout(10, 0));
        checkers = c;
        cases = new HashMap<Coord, CasePanel>();
        board = b;
        placeCases();
        inRightSide = true;
        setVisible(true);

        board.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                repaint();
            }
        });
         
    }

// REQUETES
    public Board getBoard() {
        return board;
    }
    
    public boolean inRightSide() {
        return inRightSide;
    }

// COMMANDES
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Coord c : cases.keySet()) {
            CasePanel p = cases.get(c);
            int x = c.getRow(), y = c.getCol();
            Piece piece = board.getPiece(x, y);
            if (piece != null) {
                    piece.setRectangle(new Rectangle(new Dimension(8, 8)));
                    p.setPiece(piece);
                } else {
                p.setPiece(null);
            }
            p.repaint();
        }
    }
    
    public void inverse() {
        Map<Coord, CasePanel> casesBis = new HashMap<Coord, CasePanel>();
        CasePanel p;
        board.print();
        for (int i = Board.BOARD_SIZE - 1; i >= 0; --i) {
            for (int k = 0; k < Board.BOARD_SIZE; ++k) {
                p = cases.get(new Coord(k, i));
                p.setCoord(9 -k, 9 -i);
                Piece piece = board.getPiece(9 -k, 9 -i);
                if (piece != null) {
                    piece.setRectangle(new Rectangle(new Dimension(8, 8)));
                    p.setPiece(piece);
                }
                casesBis.put(new Coord(9 -k, 9 - i), p);
                p.repaint();
            }
        }
        cases = casesBis;
        repaint();
        inRightSide = !inRightSide;
    }
    
    public void rightSide() {
        Map<Coord, CasePanel> casesBis = new HashMap<Coord, CasePanel>();
        CasePanel p;
        board.print();
        for (int i = Board.BOARD_SIZE - 1; i >= 0; --i) {
            for (int k = 0; k < Board.BOARD_SIZE; ++k) {
                p = cases.get(new Coord(k, i));
                p.setCoord(k, i);
                Piece piece = board.getPiece(k, i);
                if (piece != null) {
                    piece.setRectangle(new Rectangle(new Dimension(8, 8)));
                    p.setPiece(piece);
                }
                casesBis.put(new Coord(k, i), p);
                p.repaint();
            }
        }
        cases = casesBis;
        repaint();
        inRightSide = true;
    }

    // OUTILS
    private void placeCases() {
        CasePanel p;
        for (int i = Board.BOARD_SIZE - 1; i >= 0; --i) {
            for (int k = 0; k < Board.BOARD_SIZE; ++k) {
                p = new CasePanel(k, i, checkers);
                Piece piece = board.getPiece(k, i);
                if (piece != null) {
                    piece.setRectangle(new Rectangle(new Dimension(8, 8)));
                    p.setPiece(piece);
                }
                add(p);
                cases.put(new Coord(k, i), p);
            }
        }
    }
}
