package fr.gdna.gui;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Pseudo extends JDialog {
	String nom;
  public Pseudo(JFrame parent, String title, boolean modal){
    //On appelle le construteur de JDialog correspondant
    super(parent, title, modal);
    
    JOptionPane jop = new JOptionPane(), jop2 = new JOptionPane();
     nom = jop.showInputDialog(null, "Veuillez donnez votre pseudo !", "", JOptionPane.QUESTION_MESSAGE);
    jop2.showMessageDialog(null, "Votre pseudo est " + nom, "Identit�", JOptionPane.INFORMATION_MESSAGE);
    this.setVisible(false);   
  }
  
  public String getName(){
	  return this.nom;
  }
}