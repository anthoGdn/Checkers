package fr.gdna.gui;

import fr.gdna.util.RequestClient;
import javax.swing.JFrame;
import javax.swing.JList;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class ListFrame extends JFrame implements ListSelectionListener {

    private final JButton toSend;
    private final JList list;
    //private final ArrayList<String> arr;
    private final PrintWriter out;
    private final Checkers checkers;

    public ListFrame(PrintWriter o, Checkers c) {
        super();
        setTitle("Liste des joueurs connectés");
        setMinimumSize(new Dimension(200, 300));
        setLocation(200, 300);
        out = o;
        checkers = c;
        toSend = new JButton("Envoyer");
        StdListModel model = new StdListModel();
        list = new JList(model);
        list.setCellRenderer(new ColorRenderer(model));

        add(new JScrollPane(list), BorderLayout.CENTER);
        JPanel p = new JPanel(new BorderLayout());
        p.add(toSend, BorderLayout.EAST);
        add(p, BorderLayout.SOUTH);
        setVisible(true);

        createController();
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {

    }

    public void mouseReleased(MouseEvent e) {
        Object index = list.getSelectedValue();
    }

    public String getOpponent() {
        Object index = list.getSelectedValue();
        return index.toString();
    }

    public void addPlayer(String name, boolean isDispo) {
        ((StdListModel) list.getModel()).addElement(name, isDispo);
    }

    public void removePlayer(String name) {
        ((StdListModel) list.getModel()).removeElements(name);
    }
    
    public void isDispo(String name) {
         ((StdListModel) list.getModel()).setDispo(name, true);
    }
    
    public void noDispo(String name) {
         ((StdListModel) list.getModel()).setDispo(name, false);
    }

// OUTILS
    private void createController() {
        // On redéfini ce qui se passe quand ferme la fenetre.
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // On demande au serveur d'arreter de nous envoyer les maj de la liste des joueurs car 
                // on ne veut plus connaitre ces informations.
                out.println(RequestClient.LIST_PLAYER_END.getKey());
                setVisible(false);
            }
        });
        list.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    int index = list.locationToIndex(me.getPoint());
                    String adv = ((StdListModel) list.getModel()).getElementAt(index);
                    if (adv != null) {
                        if (!checkers.inOnlineGame()) {
                        out.println(RequestClient.PROPOSE_GAME_TO.getKey() + " " + adv);
                        } else {
                            JOptionPane.showMessageDialog(null, 
                                    "Vous faites déjà une partie en ligne, vous ne pouvez pas en faire plusieurs en même temps.", 
                                    "Information", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {

            }

            @Override
            public void mouseReleased(MouseEvent me) {

            }

            @Override
            public void mouseEntered(MouseEvent me) {

            }

            @Override
            public void mouseExited(MouseEvent me) {

            }
        });

        toSend.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                int index = list.getAnchorSelectionIndex();
                String adv = ((StdListModel) list.getModel()).getElementAt(index);
                out.println(RequestClient.PROPOSE_GAME_TO.getKey() + " " + adv);
            }
        });

    }

}
