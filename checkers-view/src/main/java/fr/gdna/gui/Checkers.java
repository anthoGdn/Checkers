package fr.gdna.gui;

import fr.gdna.model.CheckersModel;
import fr.gdna.util.Constant;
import fr.gdna.util.RequestClient;
import fr.gdna.util.RequestServer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class Checkers {
//ATTRIBUTS

    public static final int WEIGHT_BOARD = 450;
    public static final int HEIGHT_BOARD = 450;
    public static final String IA = "-ia";

    private final JFrame mainFrame;
    private ListFrame frameList;
    private JPanel interactifMenu;
    private JButton connect;
    private JButton reStartLocal;
    private JButton localGame;
    private JButton deco;
    private JButton toSeeListPlayer;
    private JButton surrend;
    private boolean isOnline;
    private boolean inOnlineGame;
    private boolean inLocalGame;
    private BoardPanel panelBoard;
    private JLabel joueur;
    private JLabel adv;

    private final CheckersModel model;
    int xSrc;
    int ysrc;
    int xDst;
    int yDst;

    private String pseudo;
    private String adversaire;
    private Socket socket;
    private String ip;
    private PrintWriter out;
    private BufferedReader in;
    private static final Pattern patternPseudo = Pattern.compile(Constant.CONSTRAIN_PSEUDO);
    private static final Pattern patternAdresse = Pattern.compile("([0-9]{1,3}\\.){3}[0-9]{1,3}");

    private final Object lock;

//CONSTRUCTEUR
    public Checkers() {
        
        try {

            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (UnsupportedLookAndFeelException e) {
           // handle exception
        }
        catch (ClassNotFoundException e) {
           // handle exception
        }
        catch (InstantiationException e) {
           // handle exception
        }
        catch (IllegalAccessException e) {
           // handle exception
        }
        
        lock = new Object();
        model = new CheckersModel();
        reinitclick();
        isOnline = false;
        inLocalGame = false;
        inOnlineGame = false;
        mainFrame = new JFrame("Checkers");
        createView();
        placeBoard();
        placeFirstMenu();
        createController();
    }

//REQUETES
    public void display() {
        mainFrame.setLocation(400, 200);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    public boolean isStarted() {
        return model.isStarted();
    }

    public boolean inLocalGame() {
        return inLocalGame;
    }

    public boolean inOnlineGame() {
        return inOnlineGame;
    }

// COMMANDES
    public void setMove(int x, int y) {
        if (xSrc == -1 && ysrc == -1) {
            xSrc = x;
            ysrc = y;
            return;
        }

        if (!inOnlineGame) {
            if (model.movePiece(xSrc, ysrc, x, y) == false) {
                promptIM("Vous ne pouvez pas déplacer ce pion.");
                reinitclick();
                return;
            }
            if (model.isFinished()) {
                String message;
                if (model.getWinner().equals(IA)) {
                    message = "l'ia a gagné";
                } else {
                    message = "vous avez gagné";
                }
                inLocalGame = false;
                fireAttributChange();
                promptIM("La partie est terminée, " + message);
            }
            xSrc = -1;
            ysrc = -1;
        } else {
            xDst = x;
            yDst = y;
            out.println(RequestClient.MOVE.getKey() + RequestServer.SEP_ARGS + xSrc
                    + RequestServer.SEP_COORD + ysrc + RequestServer.SEP_ARGS + x + RequestServer.SEP_COORD + y);

        }
    }

//OUTILS
    private void createView() {
        connect = new JButton("Se connecter");
        localGame = new JButton("Jouer en local");
        reStartLocal = new JButton("Recommencer");
        toSeeListPlayer = new JButton("Joueurs connectés");
        surrend = new JButton("Abandonner la partie");
        deco = new JButton("Deconnexion");
        joueur = new JLabel("");
        adv = new JLabel("");
        fireAttributChange();
    }

    private void placeBoard() {
        panelBoard = new BoardPanel(model.getBoard(), this);
        panelBoard.setPreferredSize(new Dimension(WEIGHT_BOARD, HEIGHT_BOARD));
        panelBoard.setBorder(javax.swing.BorderFactory.createTitledBorder("Jeu"));
        JPanel p = new JPanel(new GridBagLayout());
        JPanel q = new JPanel(new GridBagLayout());
        q.add(joueur, new GBC(0, 0, 1, 1).insets(0, 0, 0, panelBoard.getPreferredSize().width / 5).anchor(GBC.WEST).weight(1, 1));
        q.add(adv, new GBC(1, 0, 1, 1).anchor(GBC.EAST).weight(1, 0));
        p.add(q, new GBC(0, 0, 1, 1).weight(1, 0));
        p.add(panelBoard, new GBC(0, 1, 2, 2).fill(GBC.BOTH).weight(1, 1));
        mainFrame.add(p, BorderLayout.CENTER);
    }

    private void placeFirstMenu() {
        JButton[] buttons = new JButton[6];
        buttons[0] = connect;
        buttons[1] = localGame;
        buttons[2] = reStartLocal;
        buttons[3] = toSeeListPlayer;
        buttons[4] = surrend;
        buttons[5] = deco;
        int maxSize = 0;
        int sizeButton;
        for (int i = 0; i < buttons.length; ++i) {
            sizeButton = buttons[i].getPreferredSize().width;
            maxSize = (maxSize < sizeButton) ? sizeButton : maxSize;
        }
        for (int i = 0; i < buttons.length; ++i) {
            buttons[i].setPreferredSize(new Dimension(maxSize, buttons[i].getPreferredSize().height));
        }

        placeComponentMenu(buttons);
    }

    private void placeComponentMenu(JButton[] buttons) {
        JPanel p = new JPanel(new GridLayout(0, 1));
        p.setBackground(Color.black);

        for (JButton b : buttons) {
            JPanel q = new JPanel();
            q.setBackground(Color.black);
            q.add(b);
            p.add(q);
        }
        p.setBorder(BorderFactory.createTitledBorder("Menu"));

        if (interactifMenu != null) {
            mainFrame.remove(interactifMenu);
            mainFrame.setLayout(new BorderLayout());
        }
        interactifMenu = p;
        mainFrame.add(interactifMenu, BorderLayout.WEST);
    }

    private void fireAttributChange() {
        connect.setEnabled(!isOnline);
        localGame.setEnabled(!inLocalGame && !inOnlineGame);
        reStartLocal.setEnabled(inLocalGame);
        toSeeListPlayer.setEnabled(isOnline);
        surrend.setEnabled(inOnlineGame);
        deco.setEnabled(isOnline);
    }

    private void createController() {
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (isOnline) {
                    out.println(RequestClient.DECONNECTION.getKey());
                }
                System.exit(0);
            }
        });

        connect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String adresse = JOptionPane.showInputDialog(
                        null, "L'adresse du serveur ", "127.0.0.1");
                if (adresse == null) {
                    return;
                }
                Matcher matcher = patternAdresse.matcher(adresse);
                if (!matcher.matches()) {
                    promptError("Vous n'avez pas entré une ip incorrect");
                    return;
                }
                ip = adresse;

                String name = JOptionPane.showInputDialog(
                        null, "Entrer votre pseudo : ", "Pseudo", JOptionPane.QUESTION_MESSAGE);

                if (name == null) {
                    return;
                }
                matcher = patternPseudo.matcher(name);
                if (!matcher.matches()) {
                    promptError("Vous n'avez pas donneé un pseudo correct.\n"
                            + "Seul les lettres non accentuées, les chiffres et l'underscore sont autorisés");
                    return;
                }

                Thread t = new Thread(new ThreadListenServer(Checkers.this));
                t.start();
                // Il faut logiquement attendre que le Thread soit vraiment démarré avant de lancer la prochaine instruction
                // spirious wakeup
                while (!isOnline) {
                    try {
                        synchronized (lock) {
                            lock.wait();
                        }
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
                out.println(RequestClient.CONNECTION.getKey() + RequestServer.SEP_ARGS + name);
            }
        });

        localGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (model.isStarted()) {
                    model.reinitGame();
                }
                if (!panelBoard.inRightSide()) {
                    panelBoard.inverse();
                }
                model.start("p1", IA, true);
                inLocalGame = true;
                fireAttributChange();
            }
        });

        reStartLocal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                model.reinitGame();
                model.start("p1", IA, true);
                fireAttributChange();
            }
        });

        toSeeListPlayer.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // On demande la liste des joueurs connectés. Pour le test, je fais en sorte de recevoir mon propre pseudo dans la liste
                        frameList = new ListFrame(out, Checkers.this);
                        out.println(RequestClient.LIST_PLAYER_ASK.getKey());
                    }
                }
        );

        surrend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                out.println(RequestClient.SURREND.getKey());
            }
        });

        deco.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        out.println(RequestClient.DECONNECTION.getKey());
                    }
                }
        );
    }

    private void deconnection() {
        if (frameList != null) {
            frameList.setVisible(false);
        }
        isOnline = false;
        inOnlineGame = false;
        fireAttributChange();
        try {
            socket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        fireAttributChange();
        promptIM("Vous avez été déconnecté");
        adversaire = null;
        pseudo = null;
        joueur.setText("");
        adv.setText("");
    }

    private void promptIM(Object message) {
        JOptionPane.showMessageDialog(null, message, "Information", JOptionPane.INFORMATION_MESSAGE);
    }

    private void promptError(Object message) {
        JOptionPane.showMessageDialog(null, message, "Erreur", JOptionPane.ERROR_MESSAGE);
    }

    private void reinitclick() {
        xSrc = -1;
        ysrc = -1;
        xDst = -1;
        yDst = -1;
    }

    private void majCurrentPlayer(String current) {
        if (pseudo.equals(current)) {
            joueur.setBorder(BorderFactory.createLineBorder(Color.red));
            adv.setBorder(null);
        } else {
            adv.setBorder(BorderFactory.createLineBorder(Color.red));
            joueur.setBorder(null);
        }
    }
// THREAD

    class ThreadListenServer implements Runnable {

        // ATTRIBUTS
        private final Map<String, RequestServer> REQ_SERVER = new HashMap<String, RequestServer>();
        private Checkers checkers;

        ThreadListenServer(Checkers c) {
            for (RequestServer r : RequestServer.values()) {
                REQ_SERVER.put(r.getKey(), r);
            }
            checkers = c;
            try {
                socket = new Socket(c.ip, Constant.PORT_DEFAULT);

            } catch (UnknownHostException e) {
                System.err.println("Impossible de se connecter à l'adresse " + Constant.PORT_DEFAULT);
            } catch (IOException e) {
                System.err.println("Aucun serveur à l'écoute du port " + Constant.PORT_DEFAULT);
            }

            try {
                out = new PrintWriter(socket.getOutputStream(), true);
                in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
            } catch (IOException e) {
                System.err.println("Le serveur ne répond plus ");
            }
        }

        @Override
        public void run() {
            isOnline = true;
            // Il faut logiquement informer le thread principale d'une notif ici pour lui dire qu'il peut continuer ses 
            // instructions à l'endroit bloquer juste apres le start du thread.
            synchronized (lock) {
                lock.notifyAll();
            }
            while (isOnline) {
                String message = null;
                try {
                    message = in.readLine();
                    if (message == null || message.equals(RequestServer.DECONNECTION.getKey())) {
                        deconnection();
                    } else {
                        final String[] tab = message.split(RequestServer.SEP_ARGS);
                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                executeRequest(tab);
                            }
                        });
                        t.start();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                    promptIM("Un probleme est survenu lors de la lecture d'une requete du serveur");
                }
            }
        }

// OUTILS
        private void executeRequest(String[] request) {
            assert request != null;

            // On cherche la requete envoyé par le serveur
            RequestServer reqS = REQ_SERVER.get(request[0]);
            if (reqS == null) {
                //Erreur, le serveur ne peut pas envoyer une requete qui n'existe pas.
                return;
            }
            Request req = Request.valueOf(reqS.toString());
            if (req == null) {
                // req ne peut pas être null, sauf si il y a une erreur apparait dans la classe internet requete.
                return;
            }

            req.execute(checkers, request);
        }
    }

    // ENUMERATION
    public enum Request {

        CONNEXION_ACCEPT(RequestServer.CONNEXION_ACCEPT) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        c.promptIM("Vous êtes connecté");
                        c.pseudo = request[1];
                        c.joueur.setText("Votre pseudo : " + request[1]);
                        c.inOnlineGame = false;
                        c.isOnline = true;
                        c.fireAttributChange();
                    }
                },
        CONNEXION_REFUSED(RequestServer.CONNEXION_REFUSED) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        c.promptIM("La connexion a échoué");
                    }
                },
        LIST_PALYER_ANSWER(RequestServer.LIST_PALYER_ANSWER) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        if (request.length == 2) {
                            // list est la liste de tous les joueurs connecté aux serveur selon la norme :
                            // "joueur1,isCoOuNon-joueur2,isCoOouNon" etc
                            String list = request[1];
                            // Liste des joueurs du style : playersList[0] = "joueur1,isCoOuNon"  
                            // playersList[1] = "joueur2,isCoOuNon"...
                            String[] playersList = list.split(RequestServer.SEP_PLAYERS);
                            // player contiendra : player[0]= "joueur1" et player[1]= "isCoOuNon"
                            String[] player;
                            // Liste de player
                            List<String[]> players = new ArrayList<String[]>();
                            int i = 0;
                            for (String p : playersList) {
                                player = p.split(RequestServer.SEP_DISPONIBILITE);
                                players.add(player);
                            }
                            c.toSeeListPlayer.setEnabled(false);

                            for (String[] s : players) {
                                boolean isDispo = s[1].equals(RequestServer.IS_DISPO);
                                c.frameList.addPlayer(s[0], isDispo);
                            }
                        }
                        c.fireAttributChange();
                    }
                },
        LIST_PALYER_MAJ(RequestServer.LIST_PALYER_MAJ) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        if (request[1].equals(RequestServer.ADD)) {
                            c.frameList.addPlayer(request[2], true);
                        } else if (request[1].equals(RequestServer.REMOVE)) {
                            c.frameList.removePlayer(request[2]);
                        } else if (request[1].equals(RequestServer.IS_DISPO)) {
                            c.frameList.isDispo(request[2]);
                        } else if (request[1].equals(RequestServer.NO_DISPO)) {
                            c.frameList.noDispo(request[2]);
                        }
                    }
                },
        PROPOSE_GAME_FROM(RequestServer.PROPOSE_GAME_FROM) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        int ans = JOptionPane.showConfirmDialog(
                                null, request[1] + " vous propose une partie. Acceptez vous ?", "Proposition de jeu", JOptionPane.YES_NO_OPTION);
                        if (c.adversaire == null) {
                            String requ;
                            if (ans == JOptionPane.YES_OPTION) {
                                requ = RequestClient.ACCEPT_GAME.getKey() + RequestServer.SEP_ARGS + request[1];
                                c.adversaire = request[1];
                                c.adv.setText("Votre adversaire : " + request[1]);
                                c.adv.setBorder(BorderFactory.createLineBorder(Color.red));
                                c.inOnlineGame = true;
                                if (c.model.isStarted()) {
                                    c.model.reinitGame();
                                }
                                c.inLocalGame = false;
                                c.model.start(c.pseudo, c.adversaire, false);
                                c.panelBoard.inverse();
                                c.fireAttributChange();
                            } else {
                                requ = RequestClient.REFUSE_GAME.getKey() + RequestServer.SEP_ARGS + request[1];
                            }
                            c.out.println(requ);
                        }
                    }
                },
        ACCEPT_GAME_ANSWER(RequestServer.ACCEPT_GAME_ANSWER) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        c.adversaire = request[1];
                        c.inOnlineGame = true;
                        if (c.model.isStarted()) {
                            c.model.reinitGame();
                        }
                        c.inLocalGame = false;
                        if (!c.panelBoard.inRightSide()) {
                            c.panelBoard.inverse();
                        }
                        c.model.start(c.pseudo, c.adversaire, false);
                        c.surrend.setEnabled(c.inOnlineGame);
                        c.promptIM(c.adversaire + " a accepté de jouer avec vous.");
                        c.adv.setText("Votre adversaire : " + request[1]);
                        c.joueur.setBorder(BorderFactory.createLineBorder(Color.red));
                        c.fireAttributChange();
                    }
                },
        REFUSE_GAME_ANSWER(RequestServer.REFUSE_GAME_ANSWER) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        c.promptIM(request[1] + " a refuser de jouer avec vous.");
                    }
                },
        DECO_ADV(RequestServer.DECO_ADV) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        c.inOnlineGame = false;
                        c.fireAttributChange();
                        c.promptIM("Votre adversaire s'est déconnecté.");
                        c.adversaire = null;
                        c.adv.setText("");
                        c.joueur.setBorder(null);
                    }
                },
        ACCEPT_MOVE(RequestServer.ACCEPT_MOVE) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        c.model.movePiece(c.xSrc, c.ysrc, c.xDst, c.yDst);
                        c.reinitclick();
                        c.majCurrentPlayer(request[3]);
                    }
                },
        REFUSE_MOVE(RequestServer.REFUSE_MOVE) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        c.reinitclick();
                        c.promptIM("Mouvement impossible");
                    }
                },
        MOVE(RequestServer.MOVE) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        String[] src = request[1].split(RequestServer.SEP_COORD);
                        String[] dst = request[2].split(RequestServer.SEP_COORD);
                        c.model.movePiece(Integer.parseInt(src[0]), Integer.parseInt(src[1]), Integer.parseInt(dst[0]), Integer.parseInt(dst[1]));
                        c.majCurrentPlayer(request[3]);
                    }
                },
        WIN(RequestServer.WIN) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        String message;
                        if (request[1].equals(RequestServer.SURREND)) {
                            message = "Vous avez gagné car votre adversaire a abandonné !";
                        } else if (request[1].equals(RequestServer.BY_KO)) {
                            message = "Vous avez gagné !";
                        } else {
                            message = "Vous avez gagné pour une raison non déterminée (bug)";
                        }
                        c.inOnlineGame = false;
                        c.fireAttributChange();
                        JOptionPane.showMessageDialog(
                                null, message, "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
                        c.adversaire = null;
                        c.adv.setText("");
                        c.joueur.setBorder(null);
                    }
                },
        LOSE(RequestServer.LOSE) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        String message;
                        if (request[1].equals(RequestServer.SURREND)) {
                            message = "Vous avez perdu par abandon !";
                        } else if (request[1].equals(RequestServer.BY_KO)) {
                            message = "Vous avez perdu !";
                        } else {
                            message = "Vous avez perdu pour une raison non déterminée (bug)";
                        }
                        c.inOnlineGame = false;
                        c.fireAttributChange();
                        JOptionPane.showMessageDialog(
                                null, message, "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
                        c.adversaire = null;
                        c.adv.setText("");
                        c.joueur.setBorder(null);
                    }
                },
        INFORMATION(RequestServer.INFORMATION) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        String[] recevedMessage = request[1].split("-");
                        StringBuilder result = new StringBuilder();
                        for (String s : recevedMessage) {
                            result.append(s).append(" ");
                        }
                        JOptionPane.showMessageDialog(
                                null, result.toString(), "Information", JOptionPane.INFORMATION_MESSAGE);
                    }
                },
        ERROR(RequestServer.ERROR) {
                    @Override
                    public void execute(Checkers c, String[] request) {
                        JOptionPane.showMessageDialog(
                                null, request, "Erreur", JOptionPane.ERROR_MESSAGE);
                    }
                };

        // ATTRIBUTS
        RequestServer req;

        // CONSTRUCTEURS
        Request(RequestServer rc) {
            req = rc;
        }

        // REQUETES
        public RequestServer getRequestServer() {
            return req;
        }

        /**
         * Appelle une methode du thread en reponse à la requete.
         *
         * @param th
         * @param request
         */
        public abstract void execute(Checkers c, String[] request);
    }

//POINT D ENTREE
    public static void main(String[] args) {
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Checkers().display();
            }
        });
    }
}
