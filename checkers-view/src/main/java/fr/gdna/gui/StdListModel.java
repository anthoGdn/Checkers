package fr.gdna.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractListModel;

public class StdListModel extends AbstractListModel {
//ATTRIBUTS

    private final List<String> list;
    private final List<String> dispo;

//CONSTRUCTEURS
    public StdListModel() {
        list = new ArrayList<String>();
        dispo = new ArrayList<String>();
    }

//REQUETES
    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public String getElementAt(int i) {
        if (i < 0 || getSize() <= i) {
            throw new IllegalArgumentException("i n'est pas compris entre 0 et getSize()");
        }
        return list.get(i);
    }
    
    public boolean isDispo(String elem) {
        return dispo.contains(elem);
    }

// COMMANDES
    public void addElement(String element, boolean isDispo) {
        if (element == null) {
            throw new IllegalArgumentException("L'élément à  ajouter ne doit pas être null.");
        }
        list.add(element);
        if (isDispo) {
            dispo.add(element);
        }
        int length = getSize() - 1;
        fireIntervalAdded(this, length, length);

    }

    public void setElements(Collection<String> c) {
        if (c == null) {
            throw new IllegalArgumentException("La collection à  ajouter ne doit pas être null.");
        }
        list.addAll(c);
        dispo.addAll(c);
        fireContentsChanged(this, 0, list.size() - 1);
    }

    public void removeElements(String elem) {
        if (elem == null) {
            throw new IllegalArgumentException("La collection à  ajouter ne doit pas être null.");
        }
        if (list.contains(elem)) {
            int index = list.indexOf(elem);
            list.remove(elem);
            dispo.remove(elem);
            fireIntervalRemoved(this, index, index);
        }
    }
    
    public void setDispo(String name, boolean isDispo) {
        if (isDispo) {
            dispo.add(name);
        } else {
            dispo.remove(name);
        }
        fireContentsChanged(this, 0, list.size() - 1);
    }
}
