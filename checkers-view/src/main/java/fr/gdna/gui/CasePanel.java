package fr.gdna.gui;

import fr.gdna.model.Piece;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;


public class CasePanel extends JPanel {
// ATTRIBUTS

    private int x;
    private int y;
    private Piece piece;
    Checkers checkers;

// CONSTRUCTEUR    
    public CasePanel(int x, int y, Checkers c) {
        super();
        checkers = c;
        this.x = x;
        this.y = y;

        addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent me) {
//                if (checkers.inLocalGame() || checkers.inOnlineGame()) {
//                    int row = ((CasePanel) me.getComponent()).getRow();
//                    int col = ((CasePanel) me.getComponent()).getCol();
//                    checkers.setMove(row, col);
//                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
                if (checkers.inLocalGame() || checkers.inOnlineGame()) {
                    int row = ((CasePanel) me.getComponent()).getRow();
                    int col = ((CasePanel) me.getComponent()).getCol();
                    checkers.setMove(row, col);
                }
            }

            @Override
            public void mouseReleased(MouseEvent me) {

            }

            @Override
            public void mouseEntered(MouseEvent me) {

            }

            @Override
            public void mouseExited(MouseEvent me) {

            }
        });
    }

    public Piece getPiece() {
        return piece;
    }

    public int getRow() {
        return x;
    }

    public int getCol() {
        return y;
    }

    public void setPiece(Piece p) {
        piece = p;
    }

    public void setCoord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Color color;
        if ((x + y) % 2 == 0) {
            color = Color.WHITE;
        } else {
            if (checkers.isStarted()) {
                color = Color.GRAY;
            } else {
                //color = new Color((float) 0.1, (float) 0.01, (float) 0.380);
                color = Color.DARK_GRAY;
            }
        }
        setBackground(color);
        setBorder(BorderFactory.createEtchedBorder());
        Rectangle rec;
        if (piece != null && (rec = piece.getRectangle()) != null) {
            if (piece.getColor() == Piece.PieceColor.WHITE) {
                g.setColor(Color.WHITE);
            } else {
                g.setColor(Color.BLACK);
            }
            g.fillOval(getWidth() / 10, getHeight() / 10, (int) rec.getWidth() * getWidth() / 10, (int) rec.getHeight() * getHeight() / 10);

            if (piece.getType() == Piece.PieceType.KING) {
                if (piece.getColor() == Piece.PieceColor.WHITE) {
                    g.setColor(Color.BLACK);
                } else {
                    g.setColor(Color.WHITE);
                }
            }
            g.fillOval((int) (getWidth() / 2.6), (int) (getHeight() / 2.7), (int) rec.getWidth() * getWidth() / 30, (int) rec.getHeight() * getHeight() / 30);
        }
    }

}
