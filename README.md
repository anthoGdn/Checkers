# Checkers
Projet java de jeu de dames (internationnal) avec Maven.
Le projet contient 3 sous projets :
* Le modèle;
* Une interface graphique sur client lourd java (swing);
* Un serveur java.

La compilation avec Maven rencontre un petit problème. Je vous conseille donc
d'executer votre code depuis un IDE.
Le serveur se lancera sur l'adresse 127.0.0.1 par défaut.