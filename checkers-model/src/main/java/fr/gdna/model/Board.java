package fr.gdna.model;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Board extends JPanel {

    // ATTRIBUTS STATIQUES
    /**
     * Taille de coté du plateau.
     */
    public static final int BOARD_SIZE = 10;
    /**
     * Au début d'une partie, il y a toujours 3 lignes de chaque coté qui
     * contienent des pieces
     */
    public static final int FILLED_LINES = 4;

//ATTRIBUTS
    /**
     * Plateau contenant les pièces. board[x][y] désigne la pièce sur la ligne x
     * et la colonne y.
     */
    private final Piece[][] board;
    /**
     * Set des pièces blanches set des pièces noires.
     */
    private final List<Piece> whitePcs;

    private final List<Piece> blackPcs;
    /**
     * Pièces retirées du plateau
     */
    private final List<Piece> deadPcs;

    //private EventListenerList listenerns;
    private final ChangeEvent changeEvent;

// CONSTRUCTEURS
    /**
     * Un plateau de jeu.
     */
    public Board() {
        board = new Piece[BOARD_SIZE][BOARD_SIZE];
        whitePcs = new ArrayList<Piece>();
        blackPcs = new ArrayList<Piece>();
        deadPcs = new ArrayList<Piece>();

        changeEvent = new ChangeEvent(this);
    }
// REQUETES

    /**
     * La pièce en (x, y)
     *
     * @pre isInBoard(x, y)
     */
    public Piece getPiece(int row, int col) {
        if (!isInBoard(row, col)) {
            throw new IllegalArgumentException(row + "," + col + " pas sur le plateau");
        }
        return board[row][col];
    }

    /**
     * Donne la premiere piece trouvée à partir de la source jusqu'à dst.
     */
    public Piece getPieceBetween(int srcX, int srcY, int dstX, int dstY) {
        Direction d = Direction.getAlignment(srcX, srcY, dstX, dstY);
        Coord dst = new Coord(dstX, dstY);
        Piece p;
        for (Coord c = d.translated(srcX, srcY); !c.equals(dst); c = d.translated(c.getRow(), c.getCol())) {
            p = getPiece(c.getRow(), c.getCol());
            if (p != null) {
                return p;
            }
        }
        return null;
    }

    /**
     * Indique si (x, y) est sur le plateau de jeu.
     */
    public boolean isInBoard(int row, int col) {
        return 0 <= row && row < BOARD_SIZE
                && 0 <= col && col < BOARD_SIZE;
    }

    /**
     * Affiche le damier sur la sortie standard.
     */
    public void print() {
        for (int y = BOARD_SIZE - 1; y >= 0; --y) {
            System.out.print("| " + y + "  | ");
            for (int x = 0; x < BOARD_SIZE; ++x) {
                Piece p = board[x][y];
                if (p != null) {
                    System.out.print(p.getString() + " | ");
                } else {
                    System.out.print("   | ");
                }
            }
            System.out.println();
        }
        System.out.print("|    | ");
        for (int x = 0; x < BOARD_SIZE; ++x) {
            System.out.print(x + "  | ");
        }
        System.out.println("\n");
    }

    public List<Piece> getWhitePcs() {
        return whitePcs;
    }

    public List<Piece> getBlackPcs() {
        return blackPcs;
    }

    public ChangeListener[] getChangeListeners() {
        return listenerList.getListeners(ChangeListener.class);
    }

// COMMANDES
    public void addChangeListener(ChangeListener listener) {
        if (listener == null) {
            return;
        }
        listenerList.add(ChangeListener.class, listener);
    }

    public void removeChangeListener(ChangeListener listener) {
        if (listener == null) {
            return;
        }
        listenerList.remove(ChangeListener.class, listener);
    }

    /**
     * Ajoute une pièce au plateau.
     *
     * @pre isInBoard(row, col) p != null getPiece(row, col) == null
     * @pre getPiece(row, col) == p le set de couleurs de p contient p
     */
    public void addPiece(Piece p) {
    	if (p == null) {
            throw new IllegalArgumentException("p == null");
        }
        Coord c = p.getCoord();
        int row = c.getRow(), col = c.getCol();
        if (!isInBoard(row, col)) {
            throw new IllegalArgumentException(row + "," + col + " pas sur le plateau");
        }
        if (board[row][col] != null) {
            throw new IllegalStateException("Il y a déjà une pièce en " + row + "," + col);
        }
        if (p.getColor() == Piece.PieceColor.WHITE) {
            whitePcs.add(p);
        } else if (p.getColor() == Piece.PieceColor.BLACK) {
            blackPcs.add(p);
        } else {
            //impossible
            throw new IllegalStateException();
        }
        board[row][col] = p;
        fireStateChanged();
    }

    /**
     * Déplace p en (row, col)
     *
     * @pre p != null isInBoard(x, y)
     * @post getPiece(old p.getX(), old p.getY()) == null p.getX() == x p.getY()
     * == y getPiece(x, y) == p
     */
    public void movePiece(int rowSrc, int colSrc, int rowDst, int colDst) {
        if (!isInBoard(rowSrc, colSrc)) {
            throw new IllegalArgumentException(rowSrc + "," + colSrc + " pas sur le plateau");
        }
        if (!isInBoard(rowSrc, colSrc)) {
            throw new IllegalArgumentException(rowDst + "," + colDst + " pas sur le plateau");
        }
        Piece p = board[rowSrc][colSrc];
        board[rowSrc][colSrc] = null;
        board[rowDst][colDst] = p;
        Coord c = new Coord(rowDst, colDst);
        p.setCoord(c);
        fireStateChanged();
    }

    /**
     * Retire du plateau de jeu la pièce se trouvant en (row, col).
     */
    public void removePiece(int row, int col) {
        Piece p = board[row][col];
        if (p == null) {
            return;
        }

        if (p.getColor() == Piece.PieceColor.BLACK) {
            blackPcs.remove(p);
        } else if (p.getColor() == Piece.PieceColor.WHITE) {
            whitePcs.remove(p);
        } else {
            //impossible
            throw new IllegalStateException();
        }
        board[row][col] = null;
        deadPcs.add(p);
        fireStateChanged();
    }

    public void reinit() {
        for (int i = 0; i < BOARD_SIZE; ++i) {
            for (int k = 0; k < BOARD_SIZE; ++k) {
                board[i][k] = null;
            }
        }
        blackPcs.clear();
        whitePcs.clear();
        deadPcs.clear();
        fireStateChanged();
    }

//OUTILS
    
    
    protected void fireStateChanged() {
        ChangeListener[] listnrs = getChangeListeners();
        for (ChangeListener lst : listnrs) {
            lst.stateChanged(changeEvent);
        }
    }
    
    /**
     * Indique si deux cases sont alignés (diaglonalement parlant)
     *
     * @pre les deux cases font partie du plateau de jeu
     */
    private boolean alignedTogether(int rowSrc, int colSrc, int rowDst, int colDst) {
        assert isInBoard(rowSrc, colSrc) && isInBoard(rowDst, colDst);

        int difRow = Math.abs(rowDst - rowSrc);
        int difCol = Math.abs(colDst - colSrc);

        return difRow == difCol;
    }

    /**
     * Toutes les pièces de couleur c présentes sur le plateau.
     *
     * @pre c != null
     */
    private Set<Piece> allPiecesOfColor(Piece.PieceColor c) {
        if (c == null) {
            throw new IllegalArgumentException();
        }
        if (c == Piece.PieceColor.WHITE) {
            return new HashSet<Piece>(whitePcs);
        } else if (c == Piece.PieceColor.BLACK) {
            return new HashSet<Piece>(blackPcs);
        } else {
            //impossible
            throw new IllegalStateException();
        }
    }

    /**
     * Indique si (row, col) contient une pièce.
     *
     * @pre isIn(row,col)
     */
    private boolean containsPiece(int row, int col) {
        if (!isInBoard(row, col)) {
            throw new IllegalArgumentException(row + "," + col);
        }
        return board[row][col] != null;

    }

    /**
     * Indique si (row, col) contient une pièce de couleur clr.
     *
     * @pre isIn(row, col)
     */
    private boolean containsPieceOfColor(int row, int col, Piece.PieceColor clr) {
        if (!isInBoard(row, col)) {
            throw new IllegalArgumentException();
        }
        Piece p = board[row][col];
        return p != null && p.getColor() == clr;
    }

    /**
     * Indique s'il y a une piece ennemie (et seulement une piece ennemie !!)
     * entre c1 et c2. Retourne faux s'il n'y a pas de piece, ou s'il y a une
     * piece alliée
     *
     * @pre isInBoard(x1, y1) && isInBoard(x2, y2) && alignedTogether(x1, y1,
     * x2, y2) && getPiece(x1, y1) != null)
     */
    private boolean ennemyPieceBetween(int row1, int col1, int row2, int col2) {
        if (!(isInBoard(row1, col1)
                && isInBoard(row2, col2)
                && alignedTogether(row1, col1, row2, col2)
                && getPiece(row1, col1) != null)) {
            throw new IllegalArgumentException();
        }

        Piece p = getPiece(row1, col1);
        Direction d = Direction.getAlignment(row1, col1, row2, col2);
        Coord cTmp = d.translated(row1, col1);
        Coord c2 = new Coord(row2, col2);

        //on cherche la premiere piece entre x1,y1 et x2,y2
        while (!cTmp.equals(c2)
                && getPiece(cTmp.getRow(), cTmp.getCol()) == null) {
            cTmp = d.translated(cTmp.getRow(), cTmp.getCol());
        }

        //si on a atteint c2 sans trouver de piece
        if (cTmp.equals(c2)) {
            return false;
        }
        //sinon on verifie que cette piece est ennemie et qu'il n'y en a pas d'autre
        return getPiece(cTmp.getRow(), cTmp.getCol()).getColor() != p.getColor()
                && freePathBetween(row1, col1, row2, col2);
    }

    /**
     * Indique si la voie est libre en deux cases alignées (non incluses).
     *
     * @pre alignedTogether(xsrc, ysrc, xdst, ydst)
     */
    private boolean freePathBetween(int rowSrc, int colSrc, int rowDst, int colDst) {
        assert alignedTogether(rowSrc, colSrc, rowDst, colDst);

        Direction d = Direction.getAlignment(rowSrc, colSrc, rowDst, colDst);
        boolean free = true;
        int row = rowSrc + d.hor();
        int col = colSrc + d.vert();
        while (row != rowDst) {
            free = free && !containsPiece(row, col);
            row += d.hor();
            col += d.vert();
        }
        return free;
    }


//TYPES
    public enum Direction {

        TOP_RIGHT(1, 1),
        BOTTOM_RIGHT(1, -1),
        BOTTOM_LEFT(-1, -1),
        TOP_LEFT(-1, 1);

        //ATTRIBUT
        /**
         * Deplacement horizontale (de colone en colone).
         */
        private final int hor;
        /**
         * Deplacement vertical (de ligne en ligne).
         */
        private final int vert;
        /**
         * Direction opposé
         */
        private static final Map<Direction, Direction> OPPOSITES;

        static {
            OPPOSITES = new EnumMap<Direction, Direction>(Direction.class);
            OPPOSITES.put(TOP_RIGHT, BOTTOM_LEFT);
            OPPOSITES.put(BOTTOM_LEFT, TOP_RIGHT);
            OPPOSITES.put(BOTTOM_RIGHT, TOP_LEFT);
            OPPOSITES.put(TOP_LEFT, BOTTOM_RIGHT);
        }

        //CONSTRUCTEUR
        Direction(int hor, int vert) {
            this.hor = hor;
            this.vert = vert;
        }

        public int hor() {
            return hor;
        }

        public int vert() {
            return vert;
        }

        public Coord translated(int row, int col) {
            return new Coord(row + this.hor, col + this.vert);
        }

        public Direction opposate() {
            return OPPOSITES.get(this);
        }

        /**
         * Donne l'alignement diagonal entre deux pieces (TOP_LEFT, BOTTOM_LEFT,
         * BOTTOM_RIGHT)
         */
        public static Direction getAlignment(int rowScr, int colSrc, int rowDst, int colDst) {
            if (rowScr > rowDst) {
                if (colSrc > colDst) {
                    return Direction.BOTTOM_LEFT;
                } else if (colSrc < colDst) {
                    return Direction.TOP_LEFT;
                } else {
                    return null;
                }
            } else if (rowScr < rowDst) {
                if (colSrc > colDst) {
                    return Direction.BOTTOM_RIGHT;
                } else if (colSrc < colDst) {
                    return Direction.TOP_RIGHT;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

}
