package fr.gdna.model;

import java.util.ArrayList;
import java.util.List;

public class Move {
// ATTIBUTS

    private final List<Coord> move;
    private final List<Coord> jumpedList;
    private boolean priority ;

// CONTRUCTEURS
    public Move(Coord src) {
        this();
        move.add(src);
    }

    private Move() {
        move = new ArrayList<Coord>();
        jumpedList = new ArrayList<Coord>();
        priority = false;
    }

    // REQUETES
    public int size() {
        return move.size();
    }

    public boolean isEmpty() {
        return move.isEmpty();
    }

    public boolean contains(Coord c) {
        return move.contains(c);
    }

    @Override
    public String toString() {
        return move.toString();
    }

    public Move copy() {
        Move result = new Move();
        result.move.addAll(move);
        return result;
    }

    public Coord get(int i) {
        return move.get(i);
    }

    public Coord getSrc() {
        return move.get(0);
    }

    public Coord getLastCoord() {
        return move.get(move.size() - 1);
    }
    
    public boolean isPriority() {
        return priority;
    }

// COMMANDES
    public boolean add(Coord c) {
        return move.add(c);
    }

    public boolean remove(Coord c) {
        return move.remove(c);
    }

    public void clear() {
        move.clear();
    }

    public Coord set(int i, Coord c) {
        return move.set(i, c);
    }
    
    public void addJumped(Coord j) {
        jumpedList.add(j);
        priority = true;
    }
    
    public List<Coord> getJumpedList() {
        return jumpedList;
    }

}
