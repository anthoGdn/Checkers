package fr.gdna.model;

import fr.gdna.model.Board.Direction;
import fr.gdna.model.Piece.PieceColor;
import fr.gdna.model.Piece.PieceType;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckersModel {

// ATTRIBUTS
    private final static HashSet<Direction> DIR_PROHIBITED_FOR_WHITE_PAWN = new HashSet<Direction>();

    static {
        DIR_PROHIBITED_FOR_WHITE_PAWN.add(Direction.BOTTOM_LEFT);
        DIR_PROHIBITED_FOR_WHITE_PAWN.add(Direction.BOTTOM_RIGHT);
    }

    private final static HashSet<Direction> DIR_PROHIBITED_FOR_BLACK_PAWN = new HashSet<Direction>();

    static {
        DIR_PROHIBITED_FOR_BLACK_PAWN.add(Direction.TOP_LEFT);
        DIR_PROHIBITED_FOR_BLACK_PAWN.add(Direction.TOP_RIGHT);
    }

    private final Board board;
    private boolean started;
    private boolean finished;
    private String winner;
    private int currentPlayer;
    private String[] players;
    // Tous les mouvement possible à ce tour.
    private PossibleMove possibleMoves;
    private boolean againstIA;

// CONSTRUCTEURS
    public CheckersModel() {
        started = false;
        finished = false;
        winner = null;
        board = new Board();
    }

// REQUETES
    /**
     * Renvoie le tableau des deux joueurs.
     */
    public String[] getPlayers() {
        return players;
    }

    /**
     * Indique si le jeu a commencé.
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * Indique si la partie est finie.
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Renvoie le gagnant s'il y en a un. Renvoie null si match null ou partie
     * en cours.
     */
    public String getWinner() {
        return winner;
    }

    /**
     * Le joueur dont c'est le tour de jouer.
     */
    public String getCurrentPlayer() {
        return players[currentPlayer];
    }

    /**
     * Indique si on a le droit de bouger la piece de xsrc,ysrc en xdst,ydst.
     * Tous les coups possible sont dans possibleMoves.
     */
    public boolean canMovePiece(int xsrc, int ysrc, int xdst, int ydst) {
        return possibleMoves.belongTo(new Coord(xsrc, ysrc), new Coord(xdst, ydst));
    }

    /**
     * Affiche sur la sortie stantard une représentation du plateau de jeu.
     */
    public void printBoard() {
        board.print();
    }

    /**
     * Renvoie le plateau de jeu du modele.
     */
    public Board getBoard() {
        return board;
    }

    /**
     * Retourne les mouvement possible du jeu à cette instant.
     */
    public PossibleMove getPossibleMove() {
        return possibleMoves;
    }

    /**
     * Indique si on joue contre une IA.
     */
    public boolean againstIA() {
        return againstIA;
    }

// COMMANDES
    /**
     * Demarre une partie. On y entre les pseudo du joueur 1 et 2 et on indique
     * si on veut que le 2ème joueur soit une ia. p aura les pion blanc et joue
     * en premier.
     */
    public void start(String p1, String p2, boolean ia) {
        againstIA = ia;
        initBoard();
        players = new String[2];
        players[PieceColor.WHITE.getID()] = p1;
        players[PieceColor.BLACK.getID()] = p2;
        started = true;
        currentPlayer = PieceColor.WHITE.getID();
        possibleMoves = new PossibleMove();
        // On calcule tous les mouvements possibles :
        calculPossibleMove();
    }

    /**
     * Redémare une partie.
     */
    public void reinitGame() {
        board.reinit();
        players = null;
        started = false;
        finished = false;
        winner = null;
        possibleMoves = null;
        currentPlayer = 0;
    }

    /**
     * Bouge la pièce se trouvant en xsrc, ysrc vers xdst,ydst, et fait jouer si
     * elle est activée.
     */
    public boolean movePiece(int xsrc, int ysrc, int xdst, int ydst) {
        boolean move = movePieceOnePlayer(xsrc, ysrc, xdst, ydst);
        if (againstIA && !isFinished() && move) {
            while (currentPlayer == PieceColor.BLACK.getID() && !isFinished()) {
                moveIA();
            }
        }
        return move;
    }

    /**
     * Fait abandonner le joueur pl.
     *
     * @post getWinner() est l'autre joueur n'ayant pas abandonné.
     */
    public void surrend(String pl) {
        finished = true;
        winner = (pl.equals(players[0])) ? players[1] : players[1];
    }

// OUTILS
    /**
     * Bouge la pièce se trouvant en xsrc, ysrc vers xdst,ydst.
     */
    private boolean movePieceOnePlayer(int xsrc, int ysrc, int xdst, int ydst) {
        boolean changed = false;
        if (canMovePiece(xsrc, ysrc, xdst, ydst) && !finished) {
            // Le mouvement est possible, on regarde si une piece se trouve sur le chemin du mouvement pour
            // la supprimer.
            Piece pRemove = board.getPieceBetween(xsrc, ysrc, xdst, ydst);
            if (pRemove != null) {
                Coord remove = pRemove.getCoord();
                board.removePiece(remove.getRow(), remove.getCol());
            }
            board.movePiece(xsrc, ysrc, xdst, ydst);
            changed = true;
            Coord src = new Coord(xsrc, ysrc);
            possibleMoves.removeSrc(src);
            if (possibleMoves.isEmpty()) {
                // Le tour du joueur courant est fini car il n'y a lus de mouvement possible
                // On vérifie qu'il vérifie des pions au joueur adverse, c'est la fin de la partie sinon.
                checkEndGame();
                eventuallyTransformToKing();
                if (!isFinished()) {
                    //Change le joueur courant et calcul de nouveau tous les mouvements possibles
                    changeCurrentPlayer();
                }
            }
        }
        return changed;
    }

    /**
     * Fait jouer l'ia un mouvement aléatoire parmis ses coups possibles.
     */
    private void moveIA() {
        Coord src, dst;
        PossibleMove moveSet = getPossibleMove();
        int index = (int) (moveSet.size() * Math.random());
        Move m = moveSet.get(index);
        while (m.size() != 1) {
            src = m.get(0);
            dst = m.get(1);
            movePieceOnePlayer(src.getRow(), src.getCol(), dst.getRow(), dst.getCol());
            m.remove(src);
        }
    }

    /**
     * Indique si x et y ont la même parité (tous deux pairs ou tous deux
     * impairs).
     */
    private boolean haveSameParity(int x, int y) {
        return (x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0);
    }

    /**
     * Indique si la case x y est noire.
     */
    private boolean isBlackSquare(int x, int y) {
        return haveSameParity(x, y);
    }

    /**
     * Indique si la case peutt contenir un pion. (cad noire et sur le plateau)
     */
    private boolean isPlayableSquare(int x, int y) {
        return !isBlackSquare(x, y) && board.isInBoard(x, y);
    }

    /**
     * Fais passer au tour du joueur suivant.
     */
    private void changeCurrentPlayer() {
        assert possibleMoves.isEmpty();
        currentPlayer = (currentPlayer + 1) % 2;
        // On change de joueur, on doit calculer tous les coups possibles.
        calculPossibleMove();
    }

    /**
     * Place toutes les pièces sur le plateau de jeu et les ajoute dans les Set
     * (blackPcs et whitePcs) correspondants.
     */
    private void initBoard() {
        Piece p;
        //pieces blanches : la premiere est en 0,1
        int y = 0;
        int x = 1;
        while (y < Board.FILLED_LINES) {
            while (x < Board.BOARD_SIZE) {
                p = new Piece(PieceColor.WHITE, PieceType.PAWN, new Coord(x, y));
                board.addPiece(p);
                x = x + 2;
            }
            x = (x == Board.BOARD_SIZE) ? 1 : 0;
            ++y;
        }

        //pieces noires
        y = Board.BOARD_SIZE - 1;
        x = 0;
        while (y > (Board.BOARD_SIZE - 1) - Board.FILLED_LINES) {
            while (x < Board.BOARD_SIZE) {
                p = new Piece(PieceColor.BLACK, PieceType.PAWN, new Coord(x, y));
                board.addPiece(p);
                x += 2;
            }
            x = (x == Board.BOARD_SIZE) ? 1 : 0;
            --y;
        }
    }

    /**
     * Calcule l'ensemble des coups possibles du joueur courant et l'enregistre
     * dans possibleMove.
     */
    private void calculPossibleMove() {
        assert currentPlayer == PieceColor.WHITE.getID() || currentPlayer == PieceColor.BLACK.getID();
        List<Piece> pSet;
        if (currentPlayer == PieceColor.WHITE.getID()) {
            pSet = board.getWhitePcs();
        } else {
            pSet = board.getBlackPcs();
        }
        possibleMoves.clear();
        for (Piece p : pSet) {
            Coord c = p.getCoord();
            // On ajoute dans possibleMoves les mouvements possibles à ce tour.
            possibleMoves.addAll(coordPossibleMove(c.getRow(), c.getCol()));
        }
        //System.out.println("Coordonées possibles : " + possibleMoves);
    }

    /**
     * Retourne l'ensemble des mouvements possibles d'un seul pion se trouvant
     * en (x, y).
     */
    private PossibleMove coordPossibleMove(int x, int y) {
        //possibleMoves;
        PossibleMove result = new PossibleMove();
        Move move;
        Set<Jump> possibleCoord = new HashSet<Jump>();
        if (!possibleTaking(x, y, possibleCoord, board.getPiece(x, y), null)) {
            // On ne peut pas sauter de pion.
            for (Jump bM : possibleCoord) {
                move = new Move(new Coord(x, y));
                move.add(bM.getDst());
                result.add(move);
            }
        } else {
            // On peut sauter au moins un pion, il faudra faire appel à la methode getPossibleAttack.
            for (Jump bM : possibleCoord) {
                move = new Move(new Coord(x, y));
                Coord c = bM.dst;
                move.add(c);
                Coord jumped = bM.getJumped();
                move.addJumped(jumped);
                // move est devenu prioritaire car il saute un pion. Un ajout de move dans un MoveSet contenant des mouvement sans saut de piece
                // Supprimera ces mouvement de la MoveSet
                result.addAll(getPossibleAttack(move, board.getPiece(x, y)));
            }
        }
        return result;
    }

    /**
     * Retourne un MoveSet contenant tous les mouvement (Move) possibles à
     * partir d'une coordonée c. Cette methode est récursive car un pion peut
     * sauter plusieurs pions d'afillé dans le même tour. Chaque appel à cette
     * methode correspond donc à un mouvement possible dans le même tour. On
     * enregistre dans jumpedList tous les pions sautés à l'instant du mouvement
     * où nous somme dans la methode. Move est le mouvement actuelle non
     * terminé, et p est la piece à la base du mouvement.
     */
    private PossibleMove getPossibleAttack(Move move, Piece p) {
        PossibleMove result = new PossibleMove();
        result.add(move);
        Set<Jump> possibleCoord = new HashSet<Jump>();
        // On veut recupéré la direction du dernier mouvement pour interdir d'y retourner :
        int sizeMove = move.size();
        Coord srcCoord = move.get(sizeMove - 2), dstCoord = move.get(sizeMove - 1);
        int srcX = srcCoord.getRow(), srcY = srcCoord.getCol(), dstX = dstCoord.getRow(), dstY = dstCoord.getCol();
        Direction d = Direction.getAlignment(srcX, srcY, dstX, dstY);

        if (possibleTaking(dstX, dstY, possibleCoord, p, d.opposate())) {
            // On peut encore sauter une piece
            Coord jumped;
            Coord dst;
            Move moveCpy;
            List<Coord> jumpedList = move.getJumpedList();
            for (Jump bM : possibleCoord) {
                // Pour toutes les couples de coordonée (destination et pion sauté)
                jumped = bM.getJumped();
                dst = bM.getDst();
                if (!jumpedList.contains(jumped)) {
                    // Si le pion sauté n'a pas déjà été sauté durant ce tour.
                    moveCpy = move.copy();
                    moveCpy.add(dst);
                    moveCpy.addJumped(jumped);
                    // On doit rappeler cette methode car il y a encore au moins un pion à sauter
                    result.addAll(getPossibleAttack(moveCpy, p));
                }
            }
        }
        // Il n'y a plus de pion à sauté, on retourne le résultat
        return result;
    }

    /**
     * Indique si un pion p posé éventuellement à la case (x, y) peut sauter un
     * pion adversaire et remplis ensBinomes avec les Coordonées
     * pionSautés/destionations possibles. d indique la direction qu'on ne veut
     * pas testé.
     */
    private boolean possibleTaking(int x, int y, Set<Jump> ensBinomes, Piece p, Direction impossibleD) {
        assert ensBinomes != null;
        assert isPlayableSquare(x, y);
        // On ne mettra dans cet ensemble que des coordonées qui ne permettent pas de sauté un pion, 
        //le retournera seulement si on ne peut pas sauté de pion :
        Set<Jump> ensNoPriority = new HashSet<Jump>();
        boolean result = false;
        PieceColor color = p.getColor();
        if (p.getType() == PieceType.PAWN) {
            // Vu que p est un pion, on doit lui interdir les directions vers son "camp" s'il n'a personne à sauté dans ces directions :
            Set<Direction> prohibitedSetD = (currentPlayer == PieceColor.WHITE.getID()) ? DIR_PROHIBITED_FOR_WHITE_PAWN : DIR_PROHIBITED_FOR_BLACK_PAWN;
            for (Direction d : Direction.values()) {
                if (impossibleD != d) {
                    Coord c1 = d.translated(x, y);
                    // On regarde ce qu'il y a dans la coordonée dans la direction d de (x, y)
                    if (isPlayableSquare(c1.getRow(), c1.getCol())) {
                        if (pieceIsTaking(c1.getRow(), c1.getCol(), d, color)) {
                            // c1 contient une piece "sautable", on eut donc se déplacer en d.translated(c1.getRow(), c1.getCol()))
                            ensBinomes.add(new Jump(c1, d.translated(c1.getRow(), c1.getCol())));
                            result = true;
                        } else if (!result && !prohibitedSetD.contains(d)) {
                            if (board.getPiece(c1.getRow(), c1.getCol()) == null) {
                                ensNoPriority.add(new Jump(null, c1));
                            }
                        }
                    }
                }
            }
        } else {
            // Notre pion est une dame
            for (Direction d : Direction.values()) {
                if (!d.equals(impossibleD)) {
                    // impossibleD est une direction qu'on ne peut pas prendre.
                    Coord c1 = d.translated(x, y);
                    int row = c1.getRow(), col = c1.getCol();
                    if (isPlayableSquare(row, col)) {
                        p = board.getPiece(row, col);
                    }
                    while (isPlayableSquare(row, col) && p == null) {
                        if (!result) {
                            ensNoPriority.add(new Jump(null, c1));
                        }
                        c1 = d.translated(row, col);
                        row = c1.getRow();
                        col = c1.getCol();
                        if (isPlayableSquare(row, col)) {
                            p = board.getPiece(row, col);
                        }
                    }
                    if (isPlayableSquare(row, col)
                            && pieceIsTaking(c1.getRow(), c1.getCol(), d, color)) {
                        Coord c2 = d.translated(c1.getRow(), c1.getCol());
                        while (isPlayableSquare(c2.getRow(), c2.getCol()) && board.getPiece(c2.getRow(), c2.getCol()) == null) {
                            ensBinomes.add(new Jump(c1, c2));
                            c2 = d.translated(c2.getRow(), c2.getCol());
                        }
                        result = true;
                    }
                }
            }
        }
        if (!result) {
            ensBinomes.addAll(ensNoPriority);
        }
        return result;
    }

    // Permet d'indiquer si on peut prendre une piece à une coordonée (x, y)
    // dans une certaine direction avec une piece de couleur color
    private boolean pieceIsTaking(int x, int y, Direction d, PieceColor color) {
        Piece p = board.getPiece(x, y);
        if (p == null) {
            return false;
        }
        if (p.getColor() == color.opposite()) {
            Coord c2 = d.translated(x, y);
            if (isPlayableSquare(c2.getRow(), c2.getCol())) {
                if (board.getPiece(c2.getRow(), c2.getCol()) == null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Tranforme un pion du joueur courant s'il se trouve en position pour
     * devenir une dame. Cette méthode est appelée uniquement à la fin d'un tour
     * de jeu.
     */
    private void eventuallyTransformToKing() {
        Piece p;
        int k = (currentPlayer == PieceColor.WHITE.getID()) ? (Board.BOARD_SIZE - 1) : 0;
        int i = 0;
        while (i < Board.BOARD_SIZE) {
            p = board.getPiece(i, k);
            if (p != null && p.getColor().getID() == currentPlayer) {
                p.setType(PieceType.KING);
            }
            ++i;
        }
    }

    /**
     * Vérifie s'il reste des pions à l'adversaire du joueur courant et met fin
     * à la partie si c'est le cas.
     */
    private void checkEndGame() {
        if (currentPlayer == PieceColor.WHITE.getID()) {
            if (board.getBlackPcs().isEmpty()) {
                winner = players[PieceColor.WHITE.getID()];
                finished = true;
            }
        } else {
            if (board.getWhitePcs().isEmpty()) {
                winner = players[PieceColor.BLACK.getID()];
                finished = true;
            }
        }
    }

    // TYPES IMBRIQUÉS
    public class Jump {

        // Modélise un saut de piece avec la destination et la coordonée de la piece sautée.
// ATRIBUTS

        public final Coord jumped;
        public final Coord dst;

// CONSTRUCTEURS
        public Jump(Coord jumped, Coord dst) {
            this.jumped = jumped;
            this.dst = dst;
        }

// REQUETE
        public Coord getJumped() {
            return jumped;
        }

        public Coord getDst() {
            return dst;
        }

        @Override
        public String toString() {
            if (jumped != null) {
                return "jumped : " + jumped.toString() + " - destination : " + dst.toString();
            } else {
                return " destination : " + dst.toString();
            }
        }

    }

}
