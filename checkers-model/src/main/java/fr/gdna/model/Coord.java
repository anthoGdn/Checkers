package fr.gdna.model;

public class Coord {

//ATTRIBUTS
    private final int row;
    private final int col;

//CONSTRUCTEUR
    /**
     * Une coordonnée row,col.
     *
     * @post getX() == x getY() == y
     */
    public Coord(int r, int c) {
        this.row = r;
        this.col = c;
    }
//REQUETES

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass() == this.getClass()) {
            Coord c = (Coord) o;
            return c.row == this.row && c.col == this.col;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.row;
        hash = 67 * hash + this.col;
        return hash;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }
    
    @Override
    public String toString() {
        return "(" + getRow() + ", " + getCol() + ")";
    }

//COMMANDES
}
