package fr.gdna.model;

import java.awt.Rectangle;
import java.util.EnumMap;
import java.util.Map;

/**
 * Piece du jeu de dame. Possèdent -une couleur -un type -des coordonnées x et y
 * sur un plateau de jeu
 *
 * @inv getType() != null getColor() != null
 */
public class Piece {

// ATTRIBUTS
    private final PieceColor color;
    private PieceType type;
    private Coord coord;
     private Rectangle rec;

// CONSTRUCTEURS
    /**
     * Pièce de type t de couleur c.
     *
     * @pre c != null t != null
     * @post getColor() == c getType() == t
     */
    public Piece(PieceColor c, PieceType t, Coord coord) {
        if (c == null || t == null) {
            throw new IllegalArgumentException();
        }
        color = c;
        type = t;
        this.coord = coord;
    }

// REQUETES
    /**
     * Couleur de la pièce.
     */
    public PieceColor getColor() {
        return color;
    }

    /**
     * Type de la pièce.
     */
    // Donne le type de la piece : pion ou reine.
    public PieceType getType() {
        return type;
    }
    
    /**
     * Represente une piece pour le débuggage. 
     */
    public String getString() {
        String s = (getColor() == PieceColor.BLACK) ? "N" : "B";
        s += (getType() == PieceType.KING) ? "C" : "P";
        return s;
    }

    public Coord getCoord() {
        return coord;
    }
    
    @Override
    public String toString() {
        return coord + " : " + color + " - " + type;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj != null && getClass() == obj.getClass()) {
            Piece o = (Piece) obj;
            return color.equals(o.color) && type.equals(o.type) && coord.equals(o.coord);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.color != null ? this.color.hashCode() : 0);
        hash = 47 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 47 * hash + (this.coord != null ? this.coord.hashCode() : 0);
        return hash;
    }
    
    public Rectangle getRectangle() {
        return rec;
    }
    
// COMMANDES
    
    public void setRectangle(Rectangle r) {
        rec = r;
    }
    
    /**
     * Fixe le type de la pièce
     *
     * @pre t != null
     * @post getType() == t
     */
    public void setType(PieceType t) {
        if (t == null) {
            throw new IllegalArgumentException();
        }
        this.type = t;
    }
    
     public void setCoord(Coord coord) {
        this.coord = coord;
    }

// CLASSES INTERNES 
    public enum PieceType {
        
        PAWN, KING;
    }

    public enum PieceColor {
        
        BLACK(1), WHITE(0);
        private static final Map<PieceColor, PieceColor> OPPOSITES;
        static {
            OPPOSITES = new EnumMap<PieceColor, PieceColor>(PieceColor.class);
            OPPOSITES.put(BLACK, WHITE);
            OPPOSITES.put(WHITE, BLACK);
        }
        
        private final int id;
        
        PieceColor(int i) {
            id = i;
        }

        public PieceColor opposite() {
            return OPPOSITES.get(this);
        }
        
        public int getID() {
            return id;
        }
    }
}
