package fr.gdna.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Classe représentant des ensembles de move ayant tous la même taille. Le but
 * des ces ensembles est d'avoir les éléments les plus longs possibles. Sert
 * pour le calcul des coups obligatoires. Lors de l'ajout d'un Move, celui ci
 * est ajouté seulement si il est strictement plus long (les autres sont alors
 * retirés) ou de taille égale à ceux présents.
 *
 */
public class PossibleMove {

    //ATTRIBUTS
    private List<Move> moveSet;
    private int eltLength;
    private boolean priority;

    //CONSTRUCTEUR
    public PossibleMove() {
        moveSet = new ArrayList<Move>();
        eltLength = 0;
        priority = false;
    }

    //REQUETES
    /**
     * Taille des éléments de l'ensemble.
     */
    public int elementLength() {
        return eltLength;
    }

    /**
     * Itérateur sur les éléments de l'ensemble.
     */
    public Iterator<Move> iterator() {
        return moveSet.iterator();
    }
    
    public Move get(int i) {
        assert i <moveSet.size();
        return moveSet.get(i);
    }

    /**
     * Nombre d'éléments dans l'ensemble.
     */
    public int size() {
        return moveSet.size();
    }

    public boolean contains(Move m) {
        return moveSet.contains(m);
    }

    public boolean belongTo(Coord src, Coord dst) {
        for (Move l : moveSet) {
            if (l.get(0).equals(src) && l.get(1).equals(dst)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return moveSet.isEmpty() || eltLength == 0 || eltLength == 1;
    }

    @Override
    public String toString() {
        return moveSet.toString() + " - priority : " + priority + " | eltNb : " + elementLength();
    }

    //COMMANDES
    /**
     * Ajoute le Move m au set selon les règles d'ajout de la classe.
     */
    public boolean add(Move m) {
        if (!priority && m.isPriority()) {
            moveSet.clear();
            moveSet.add(m);
            eltLength = m.size();
            priority = true;
            return true;
        } else if (!m.isPriority() && priority) {
            return false;
        }
        if (m.size() > eltLength) {
            moveSet.clear();
            moveSet.add(m);
            eltLength = m.size();
        } else if (m.size() == eltLength) {
            moveSet.add(m);
        } else {
            return false;
        }
        return true;
    }

    /**
     * Ajoute tous les elements de ms à this selon les règles d'ajout de la
     * classe.
     */
    public boolean addAll(PossibleMove ms) {
        if (priority && !ms.priority) {
            return false;
        } else if (!priority && ms.priority) {
            priority = true;
            moveSet = ms.moveSet;
            eltLength = ms.elementLength();
            return true;
        }

        if (ms.elementLength() > eltLength) {
            moveSet = ms.moveSet;
            eltLength = ms.elementLength();
        } else if (ms.elementLength() == eltLength) {
            moveSet.addAll(ms.moveSet);
        } else {
            return false;
        }
        return true;
    }

    public void removeSrc(Coord src) {
        boolean modif = false;
        Move m;
        int size = moveSet.size();
        for (int i = 0; i < size; ++i) {
            m = moveSet.get(i);
            if (m.getSrc().equals(src)) {
                m.remove(src);
                modif = true;
            } else {
                moveSet.remove(m);
                --i;
                --size;
            }
        }
        assert modif;
        if (modif) {
            --eltLength;
        }
    }

    /**
     * Efface l'ensemble.
     */
    public void clear() {
        moveSet.clear();
        eltLength = 0;
        priority = false;
    }

}
