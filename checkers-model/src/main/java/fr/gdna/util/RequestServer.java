package fr.gdna.util;

public enum RequestServer {

    // Reponse après une demande de connexion
    CONNEXION_ACCEPT("connection_accept", 1),
    CONNEXION_REFUSED("connection_refused", 1), /* raison */
    //Liste de tous les joueurs connectés
    LIST_PALYER_ANSWER("lpAnswer", 1),
    //Ajout ou retirer - pseudo
    LIST_PALYER_MAJ("lpMaj", 2),
    //Proposition de jeu à pseudo
    PROPOSE_GAME_FROM("pgFrom", 1),
    // Reponse à envoyer au joueur
    ACCEPT_GAME_ANSWER("agAnswer", 1),
    //Reponse à envoyer au joueur
    REFUSE_GAME_ANSWER("rgAnswer", 1),
    // L'adversaire s'est déconnecté en pleins milieu de partie
    DECO_ADV("deco_adv", 0),
    // Validation de mouvement avec les deux coordonées du mouvements et 
    // le nom du joueur à qui c'est le tour de jouer
    ACCEPT_MOVE("acceptMove", 3),
    // Refus du mouvement
    REFUSE_MOVE("refuseMove", 3),
    // Coordonee du mouvement à envoyer à l'adversaire et le joueur à qui c'est le tour. 
    // Ex: srcX-srcY dstX-dstY Axelle 
    MOVE("move", 3),
    // Partie gagnée. L'argument sera SURREND ou WIN_BY_KO
    WIN("gameWin", 1),
    // Partie perdu. L'argument sera SURREND ou WIN_BY_KO
    LOSE("gameLose", 1),
    INFORMATION("inf", 1),
    // Erreur grave, suivie d'une déconnexion du client au serveur. Un argument : la raison
    ERROR("error", 1),
    // Deconnexion validé
    DECONNECTION("deconnection", 0);
    

// ATTRIBUTS
    public final static String ADD = "add";
    public final static String REMOVE = "remove";
    public final static String IS_DISPO = "isDispo";
    public final static String NO_DISPO = "noDispo";
    public final static String SURREND = "surrend";
    public final static String BY_KO = "winByKO";
    public final static String SEP_ARGS = " ";
    public final static String SEP_PLAYERS = "-";
    public final static String SEP_COORD = "-";
    public final static String SEP_DISPONIBILITE = ",";
    
    
    private final String type;
    private final int nbArg;

// CONSTRUCTEURS
    private RequestServer(String s, int nb) {
        type = s;
        nbArg = nb;
    }

// REQUETES
    public String getKey() {
        return type;
    }

    public int getNbArg() {
        return nbArg;
    }
}
