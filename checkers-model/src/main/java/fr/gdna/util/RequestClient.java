package fr.gdna.util;

public enum RequestClient {

    //Demande de connexion avec nomDuJoueur
    CONNECTION("connection", 1),
    //Demande de la liste des joueurs
    LIST_PLAYER_ASK("lpAsk", 0),
    //Fin des maj de la liste
    LIST_PLAYER_END("lpEnd", 0),
    // avec un pseudo
    PROPOSE_GAME_TO("pgTo", 1),
    // Acceptation d'un joueur à une proposition de jeu. Contient le pseudo du joueur à qui on répond
    ACCEPT_GAME("accGame", 1),
    // Refus d'un joueur à une proposition de jeu. Contient le pseudo du joueur à qui on répond
    REFUSE_GAME("refGame", 1),
    // Demande de mouvement avec une coordonée
    MOVE("move", 2),
    // Demande d'abandon
    SURREND("surrend", 0),
    DECONNECTION("deconnection", 0);

    // ATRIBUTS
    
    private final String type;
    private final int nbArgPermitted;

    // CONSTRUCTEURS			
    RequestClient(String s, int nb) {
        type = s;
        nbArgPermitted = nb;
    }

    // REQUETES		
    public String getKey() {
        return type;
    }

    public int getNbArgPermitted() {
        return nbArgPermitted;
    }
}