
package fr.gdna.util;

public class Constant {
    
    public static final String SIZE_PSEUDO = "15";
    /**
     * Contrainte des noms d'utilisateur :
     */
   public static final String CONSTRAIN_PSEUDO = "[a-zA-Z][[a-zA-Z]*_*[0-9]]{0," + SIZE_PSEUDO + "}";
   /**
     * Contrainte des coordonnées :
     */
   public static final String CONSTRAIN_COORD = "[0-9]-[0-9]";
   public static final int PORT_DEFAULT = 2000;
    
}
