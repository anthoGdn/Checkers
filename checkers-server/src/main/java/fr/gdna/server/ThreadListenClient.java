package fr.gdna.server;

import fr.gdna.model.CheckersModel;
import fr.gdna.util.Constant;
import fr.gdna.util.RequestClient;
import fr.gdna.util.RequestServer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
 * Cette classe est le thread qui s'occupe d'un client unique et qui accède à toute ses requetes.
 * Elle peut créer à la demande d'un client une partie de dame et gere les commandes
 */
public class ThreadListenClient implements Runnable {

    /*
     * Ajouter un propertyChangeListener pour envoyer la liste des joueurs co à chaque changement de cette liste du serveur.
     */
// ATTRIBUTS
    private Server server;
    private Player player;
    private String adversary;
    private CheckersModel game;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private final List<String> advPropose;
    private volatile boolean stopped;

    private final static Pattern patternUN = Pattern.compile(Constant.CONSTRAIN_PSEUDO);
    private final static Pattern patternCoord = Pattern.compile(Constant.CONSTRAIN_COORD);

    private interface ExecuteRequest {
        void execute(String[] request);
    }
    private final Map<RequestClient, ExecuteRequest> execut;

// CONSTRUCTEURS
    public ThreadListenClient(Socket skt, Server s) {
        assert skt != null && s != null;
        server = s;
        socket = skt;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
        }
        advPropose = new ArrayList<String>();
        stopped = true;

        execut = new EnumMap<RequestClient, ExecuteRequest>(RequestClient.class);
        // Rempli execut en définissant une requetes execut à chaque requete client.
        initExecut();
    }

// REQUETES	
    public String getAdversary() {
        return adversary;
    }

    public CheckersModel getGame() {
        return game;
    }

    public boolean canStartGame(String adv) {
        return searchAdvPropose(adv);
    }

    /*
     * Le thread tourne en boucle, il lit la requete du client et execute une série d'action défini dans une methode
     * associé à la requete recu.
     */
    @Override
    public void run() {
        try {
            stopped = false;
            String message = null;
            while (!stopped) {
                message = in.readLine();
                
                if (message == null) {
                    stopped = true;
                } else {
                    System.out.println(message);
                    // On découpe le message entre chaque espace.
                    final String[] request = message.split(RequestServer.SEP_ARGS);
                    if (request[0].equals(RequestClient.DECONNECTION.getKey())) {
                        stopped = true;
                    } else {
                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                executionRequest(request);
                            }
                        });
                        t.start();
                    }
                }
            }
            deconnection();
        } catch (IOException e) {
            // Arreter le Thread proprement.
            out.println(RequestServer.ERROR.getKey() + RequestServer.SEP_ARGS + "Toutes-nos-excuse,-"
                    + "une-erreur-provoquant-votre-déconexion-est-survenue.");
            deconnection();
        }
    }
    
    public void advDeco() {
        writeToClient(RequestServer.DECO_ADV.getKey());
        gameEnd();
    }

    public void writeToClient(String s) {
        out.println(s);
    }

// COMMANDES
    public void setGame(CheckersModel g, String adv) {
        adversary = adv;
        game = g;
         server.notifSeeyingList(player.getName(), RequestServer.LIST_PALYER_MAJ.getKey() + RequestServer.SEP_ARGS 
                    + RequestServer.NO_DISPO + RequestServer.SEP_ARGS + player.getName());
    }

    public int startGame(String fromPlayer) {
        if (fromPlayer == null) {
            return -1;
        }
        if (searchAdvPropose(fromPlayer)) {
            adversary = fromPlayer;
            advPropose.clear();
            game = new CheckersModel();
            game.start(player.getName(), server.getPlayer(adversary.hashCode()).getName(), false);
            writeToClient(RequestServer.ACCEPT_GAME_ANSWER.getKey() + RequestServer.SEP_ARGS + fromPlayer);
            advPropose.clear();
            server.removePlayerIsWaiting(player);
            server.notifSeeyingList(player.getName(), RequestServer.LIST_PALYER_MAJ.getKey() + RequestServer.SEP_ARGS 
                    + RequestServer.NO_DISPO + RequestServer.SEP_ARGS + player.getName());
            return 0;
        }
        return -1;
    }

    public void proposeGame(String adv) {
        advPropose.add(adv);
        writeToClient(RequestServer.PROPOSE_GAME_FROM.getKey() + " " + adv);
    }

    public void refuseGameFrom(String fromPlayer) {
        if (searchAdvPropose(fromPlayer)) {
            advPropose.remove(fromPlayer);
            writeToClient(RequestServer.REFUSE_GAME_ANSWER.getKey() + RequestServer.SEP_ARGS + player.getName());
        }
    }

    public void gameEnd() {
        game = null;
        adversary = null;
        server.addPlayerIsWaiting(player);
         server.notifSeeyingList(player.getName(), RequestServer.LIST_PALYER_MAJ.getKey() + RequestServer.SEP_ARGS 
                    + RequestServer.IS_DISPO + RequestServer.SEP_ARGS + player.getName());
    }

// OUTILS
    private void executionRequest(String[] request) {
        assert request != null;

        // On cherche l'énumeration du type de demande
        RequestClient reqC = server.getRequestClient(request[0]);
        if (reqC == null) {
            writeInformation("La-requete-" + request[0] + "-n'existe-pas.");
            return;
        }
        execut.get(reqC).execute(request);
    }

    private void writeToClientRequest(String[] tab) {
        String req = new String();
        for (String s : tab) {
            req += s + " ";
        }
        out.println(req);
    }
    
    private void writeInformation(String s) {
        out.println(RequestServer.INFORMATION.getKey() + RequestServer.SEP_ARGS + s);
    }

    private void writeToAdversary(String reqAdv) {
        server.writeTo(adversary.hashCode(), reqAdv);
    }

    // Vérifie que le pseudo est correct.
    private boolean checkUserName(String userName) {
        Matcher m = patternUN.matcher(userName);
        return m.matches();
    }

    private boolean searchAdvPropose(String p) {
        for (String s : advPropose) {
            if (p.equals(s)) {
                return true;
            }
        }
        return false;
    }

    private void setPlayer(Player p) {
        assert p != null;
        player = p;
        server.addPlayer(p, socket, this);
        server.addPlayerIsWaiting(p);
         server.notifSeeyingList(player.getName(), RequestServer.LIST_PALYER_MAJ.getKey() + RequestServer.SEP_ARGS 
                    + RequestServer.ADD + RequestServer.SEP_ARGS + player.getName());
    }

    private void connection(String[] request) throws IOException {
        assert request != null;
        String userName = request[1];
        if (!checkUserName(userName)) {
             out.println(RequestServer.CONNEXION_REFUSED.getKey() + RequestServer.SEP_ARGS + "Votre-pseudo-n'est-pas-correct");
            return;
        }
        //Request doit contenir: [0]: type de requete, [1]: le pseudo
        String ip = socket.getInetAddress().getHostAddress();
        // Il faudra vérifier qu'aucun autre joueur n'est connecté au jeu avec cette ip.
        Player p = new Player(userName, ip);
        String ans = RequestServer.CONNEXION_REFUSED.getKey() + RequestServer.SEP_ARGS + "Le-pseudo-existe-déjà-:-" + userName;
        if (server.getPlayer(p.hashCode()) == null) {
            ans = RequestServer.CONNEXION_ACCEPT.getKey() + RequestServer.SEP_ARGS + userName;
            setPlayer(p);
//            server.notifSeeyingList(userName, RequestServer.LIST_PALYER_MAJ.getKey() 
//                    + RequestServer.SEP_ARGS + RequestServer.ADD + RequestServer.SEP_ARGS + userName);
        }
        out.println(ans);
    }

    private void listPlayerAsk() {
        // Il n'y a que le type de demande dans request
        server.addPlayerIsSeeingList(player);
        out.println(RequestServer.LIST_PALYER_ANSWER.getKey()
                + RequestServer.SEP_ARGS + playerListPackage());
    }

    private String playerListPackage() {
        String res = new String();
        String co = null;

        for (Player p : server.getPlayers()) {
            if (!p.equals(player)) {
                co = RequestServer.IS_DISPO;
                if (!server.playerIsWaiting(p.hashCode())) {
                    co = RequestServer.NO_DISPO;
                }
                res += p.getName() + RequestServer.SEP_DISPONIBILITE + co + RequestServer.SEP_PLAYERS;
            }
        }
        return res;
    }

    private void listPlayerEnd() {
        server.removePlayerIsSeeingList(player);
    }

    private void proposeGameTo(String[] request) {
        assert request != null;

        String adv = request[1];
        int advCode = adv.hashCode();
        if (server.getPlayer(advCode) == null) {
            writeInformation("Le-joueur-" + adv + "-n'est-pas-connecté");
            return;
        }
        if (!server.playerIsWaiting(advCode)) {
            writeInformation("Le-joueur-" + adv + "-n'est-pas-disponible");
            return;
        }
        if (!advPropose.add(adv)) {
            writeInformation("La-demande-a-déjà-été-envoyé");
            return;
        }
        server.proposeGame(advCode, player.getName());
    }

    private void acceptGame(String[] request) {
        assert request != null;
        String adv = request[1];

        if (!advPropose.contains(adv)) {
            writeInformation("La-proposition-de-jeu-de-l'adversaire-n'est-plus-valide");
            return;
        }

        if (server.acceptGame(adv, player.getName()) == -1) {
            writeInformation("La-partie-n'a-pas-pu-être-lancé.");
            return;
        }
        writeInformation("La-partie-est-lancé,-bonne-partie.");
    }

    private void refuseGame(String[] request) {
        assert request != null;
        server.refuseGame(request[1].hashCode(), player.getName());
    }

    private void move(String[] request) {
        assert request != null;
        if (!game.getCurrentPlayer().equals(player.getName())) {
            writeToClient(RequestServer.REFUSE_MOVE.getKey()
                    + RequestServer.SEP_ARGS + request[1] + RequestServer.SEP_ARGS + request[2]);
            return;
        }
        // La requete est de type: move srcX-srcY dstX-dstY
        String[] src = request[1].split(RequestServer.SEP_COORD);
        String[] dst = request[2].split(RequestServer.SEP_COORD);

        int srcX = Integer.parseInt(src[0]);
        int srcY = Integer.parseInt(src[1]);
        int dstX = Integer.parseInt(dst[0]);
        int dstY = Integer.parseInt(dst[1]);
        String[] req = new String[1];
        String reqAdv;
        if (!game.canMovePiece(srcX, srcY, dstX, dstY)) {
            req[0] = RequestServer.REFUSE_MOVE.getKey();
        } else {
            game.movePiece(srcX, srcY, dstX, dstY);
            req[0] = RequestServer.ACCEPT_MOVE.getKey();
            reqAdv = RequestServer.MOVE.getKey() + " " + request[1] + RequestServer.SEP_ARGS + request[2]
                    + RequestServer.SEP_ARGS + game.getCurrentPlayer();
            writeToAdversary(reqAdv);
        }
        writeToClient(req[0] + RequestServer.SEP_ARGS + request[1] 
                + RequestServer.SEP_ARGS + request[2] + RequestServer.SEP_ARGS + game.getCurrentPlayer());

        if (game.isFinished()) {
            req = new String[2];
            {
                req[0] = RequestServer.WIN.getKey();
                req[1] = RequestServer.BY_KO;
            }
            writeToClientRequest(req);
            reqAdv = RequestServer.LOSE.getKey() + RequestServer.SEP_ARGS + RequestServer.BY_KO;

            writeToAdversary(reqAdv);

            server.informAdvGameEnd(adversary.hashCode());
            gameEnd();
        }
    }

    private void surrend(String[] request) {
        assert request != null;
        String[] req = new String[2];
        {
            req[0] = RequestServer.WIN.getKey();
            req[1] = RequestServer.SURREND;
        }
        writeToAdversary(req[0] + RequestServer.SEP_ARGS + req[1]);

        req[0] = RequestServer.LOSE.getKey();
        req[1] = RequestServer.SURREND;
        writeToClientRequest(req);

        server.informAdvGameEnd(adversary.hashCode());
        gameEnd();
    }

    private void deconnection() {
        if (player != null && player.getName() != null) {
            if (adversary != null) {
                server.infAdvDeco(adversary.hashCode());
            }
            String userName = player.getName();
            server.notifSeeyingList(userName, RequestServer.LIST_PALYER_MAJ.getKey()
                    + RequestServer.SEP_ARGS + RequestServer.REMOVE + RequestServer.SEP_ARGS + userName);
            server.removePlayer(player);
        }

        // Déjà vrai normalement
        stopped = true;
        // On informe le client qu'il a té déconnecté et qu'il peut fermer sa socket proprement.
        writeToClient(RequestServer.DECONNECTION.getKey());
    }

    /**
     * Remplis execut de ExecuteRequest en définissant une methode execut à chaque requete client.
     */
    private void initExecut() {
        execut.put(RequestClient.CONNECTION, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player != null) {
                    out.println(RequestServer.CONNEXION_REFUSED.getKey() + RequestServer.SEP_ARGS + 
                            "Vous-êtes-déjà-connecté-je-ne-peux-pas-executer-votre-requete-" + request[0]);
                    return;
                }
                int nbArg = RequestClient.CONNECTION.getNbArgPermitted();
                if (request.length - 1 != nbArg) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }

                try {
                    connection(request);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        execut.put(RequestClient.LIST_PLAYER_ASK, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player == null) {
                    writeInformation("Il-faut-être-connecté-pour-pouvoir-espérer-avoir-une-reponse-à-" + request[0]);
                    return;
                }
                int nbArg = RequestClient.LIST_PLAYER_ASK.getNbArgPermitted();
                if (request.length - 1 != nbArg) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }
                if (server.playerIsSeeingList(player.hashCode())) {
                    writeInformation("Erreur,-vous-ne-pouvez-pas-acceder-à-la-requete-" + request[0]);
                    return;
                }

                listPlayerAsk();
            }
        });

        execut.put(RequestClient.LIST_PLAYER_END, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player == null) {
                    writeInformation("Il-faut-être-connecté-pour-pouvoir-espérer-avoir-une-reponse-à-" + request[0]);
                    return;
                }
                int nbArg = RequestClient.LIST_PLAYER_END.getNbArgPermitted();
                if (request.length - 1 != nbArg) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }
                listPlayerEnd();
            }
        });

        execut.put(RequestClient.PROPOSE_GAME_TO, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player == null) {
                    writeInformation("Il-faut-être-connecté-pour-pouvoir-espérer-avoir-une-reponse-à-" + request[0]);
                    return;
                }
                if (request.length - 1 != RequestClient.PROPOSE_GAME_TO.getNbArgPermitted()) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }
                proposeGameTo(request);
            }
        });

        execut.put(RequestClient.ACCEPT_GAME, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player == null) {
                    writeInformation("Il-faut-être-connecté-pour-pouvoir-espérer-avoir-une-reponse-à-" + request[0]);
                    return;
                }
                int nbArg = RequestClient.ACCEPT_GAME.getNbArgPermitted();
                if (request.length - 1 != nbArg) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }
                acceptGame(request);
            }
        });

        execut.put(RequestClient.REFUSE_GAME, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player == null) {
                    writeInformation("Il-faut-être-connecté-pour-pouvoir-espérer-avoir-une-reponse-à-" + request[0]);
                    return;
                }
                int nbArg = RequestClient.REFUSE_GAME.getNbArgPermitted();
                if (request.length - 1 != nbArg) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }
                refuseGame(request);
            }
        });

        execut.put(RequestClient.MOVE, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player == null) {
                    writeInformation("Il-faut-être-connecté-pour-pouvoir-espérer-avoir-une-reponse-à-" + request[0]);
                    return;
                }
                int nbArg = RequestClient.MOVE.getNbArgPermitted();
                if (request.length - 1 != nbArg) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }
                Matcher m1 = patternCoord.matcher(request[1]);
                Matcher m2 = patternCoord.matcher(request[2]);
                if (!m1.matches() && !m2.matches()) {
                    writeInformation("Erreur,-le-format-de-la-coordonée-n'est-pas-respectée.");
                    return;
                }
                move(request);
            }
        });

        execut.put(RequestClient.SURREND, new ExecuteRequest() {
            @Override
            public void execute(String[] request) {
                assert request != null;
                if (player == null) {
                    writeInformation("Il-faut-être-connecté-pour-pouvoir-espérer-avoir-une-reponse-à-" + request[0]);
                    return;
                }
                int nbArg = RequestClient.SURREND.getNbArgPermitted();
                if (request.length - 1 != nbArg) {
                    writeInformation("Erreur,-le-nombre-d'argument-dans-la-requete-n'est-pas-respecté.");
                    return;
                }
                surrend(request);
            }
        });
    }

}
