package fr.gdna.server;

import fr.gdna.util.Constant;
import fr.gdna.util.RequestClient;
import fr.gdna.util.RequestServer;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;


public class Server {

//ATTRIBUTS
    // Port pas défaut
    public static final int BACKLOG_DEFAULT = 50;

    // Enregistre les joueurs.
    private final ConcurrentMap<Integer, Player> players;

    // Enregistre les sockets associés à un joueur.
    // private final Map<Integer, Socket> sockets;
    // Enregistre les threads associés à un joueur.
    private final ConcurrentMap<Integer, ThreadListenClient> threads;

    // Joueurs attendant une partie
    private final CopyOnWriteArraySet<Integer> isWaiting;

    // Joueurs désirant afficher la liste des adversaires
    private final CopyOnWriteArraySet<Integer> isSeeingList;

    // Map contenant toutes les requetes clients avec comme clé leurs clés.
    private final Map<String, RequestClient> reqClient;

//CONSTRUCTEUR
    public Server() {
        players = new ConcurrentHashMap<Integer, Player>();
        threads = new ConcurrentHashMap<Integer, ThreadListenClient>();

        isWaiting = new CopyOnWriteArraySet<Integer>();
        isSeeingList = new CopyOnWriteArraySet<Integer>();

        reqClient = new HashMap<String, RequestClient>();
        {
            for (RequestClient r : RequestClient.values()) {
                reqClient.put(r.getKey(), r);
            }
        }
    }

//REQUETES
    /**
     * Retourne les joueurs connectés.
     */
    public synchronized List<Player> getPlayers() {
        List<Player> l = new ArrayList<Player>();
        for (Player p : players.values()) {
            l.add(p);
        }
        return l;
    }

    /**
     * Retourne la Requete client dont s est la clé.
     */
    public RequestClient getRequestClient(String s) {
        if (s == null) {
            throw new AssertionError();
        }
        return reqClient.get(s);
    }

    /**
     * Indique si le joueur (dont le code de hashage de son pseudo est code) est
     * en attente d'aversaire.
     */
    public synchronized boolean playerIsWaiting(int code) {
        return isWaiting.contains(code);
    }

    /**
     * Indique si le joueur (dont le code de hash de son pseudo est code) attend
     * des notifications du serveur pour l'informer du changement du contenu de
     * la liste des joueurs.
     */
    public synchronized boolean playerIsSeeingList(int code) {
        return isSeeingList.contains(code);
    }

    /**
     * Retourne le joueur dont le code de hashage de son pseudo est code.
     */
    public synchronized Player getPlayer(int code) {
        return players.get(code);
    }

    /**
     * On demander à getThread(toPlayer) de proposer une partie à son client de
     * la part de playerFrom.
     */
    public void proposeGame(int toPlayer, String playerFrom) {
        getThread(toPlayer).proposeGame(playerFrom);
    }

    /**
     * On demander à getThread(toPlayer) d'écrire req à son client.
     */
    public void writeTo(int toPlayer, String req) {
        getThread(toPlayer).writeToClient(req);
    }

    /**
     * fromPlayer accepte une partie de toPlayer. On vérifie la légitimité de la
     * requete du coté de toPlayer (il faut qu'il ai proposé une partie) et si
     * c'est validé, on demande aux deux threads de lancer la partie.
     */
    public synchronized int acceptGame(String toPlayer, String fromPlayer) {
        ThreadListenClient thToPlayer = getThread(toPlayer.hashCode());
        if (!thToPlayer.canStartGame(fromPlayer)) {
            return -1;
        }
        thToPlayer.startGame(fromPlayer);
        getThread(fromPlayer.hashCode()).setGame(thToPlayer.getGame(), toPlayer);
        isWaiting.remove(toPlayer.hashCode());
        isWaiting.remove(fromPlayer.hashCode());
        return 0;
    }

    /**
     * On demander à getThread(toPlayer) de refuser une partie à son client de
     * la part de playerFrom.
     */
    public synchronized void refuseGame(int toPlayer, String fromPlayer) {
        getThread(toPlayer).refuseGameFrom(fromPlayer);
    }

    /**
     * Notifie tous les joueurs qui veulent avoir des notif de la requete req.
     */
    public synchronized void notifSeeyingList(final String userFrom, final String req) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (Integer i : isSeeingList) {
                    if (i != userFrom.hashCode()) {
                        threads.get(i).writeToClient(req);
                    }
                }
            }
        });
        t.start();
    }

//COMMANDES
    /**
     * Ajoute un joueur p à la liste des joueurs connecté et le met.
     */
    public synchronized void addPlayer(Player p, Socket s, ThreadListenClient th) {
        if (p == null || s == null) {
            throw new IllegalArgumentException();
        }
        int c = p.hashCode();
        players.put(c, p);
        threads.put(c, th);
    }

    /**
     * Ajoute le joueur p dans la liste des joueurs disponible.
     */
    public synchronized void addPlayerIsWaiting(Player p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        if (!isWaiting.contains(p.hashCode())) {
            isWaiting.add(p.hashCode());
//            notifSeeyingList(p.getName(), RequestServer.LIST_PALYER_MAJ + RequestServer.SEP_ARGS 
//                + RequestServer.IS_DISPO + RequestServer.SEP_ARGS + p.getName());
        }
    }

    /**
     * Ajoute p dans la liste des joueurs voulant reçevoir des notifications.
     */
    public synchronized void addPlayerIsSeeingList(Player p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        isSeeingList.add(p.hashCode());
    }

    /**
     * Supprime p du serveur.
     */
    public synchronized void removePlayer(Player p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        removePlayerIsSeeingList(p);
        removePlayerIsWaiting(p);
        threads.remove(p.hashCode());
        players.remove(p.hashCode());
    }

    /**
     * Supprime p des joueurs disponible.
     */
    public synchronized void removePlayerIsWaiting(Player p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        isWaiting.remove(p.hashCode());
        notifSeeyingList(p.getName(), RequestServer.LIST_PALYER_MAJ + RequestServer.SEP_ARGS
                + RequestServer.NO_DISPO + RequestServer.SEP_ARGS + p.getName());
    }

    /**
     * Supprime p des joueurs voulant recevoir des notifications.
     */
    public synchronized void removePlayerIsSeeingList(Player p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        isSeeingList.remove(p.hashCode());
    }

    /**
     * Informe adv que son adversaire s'est déconnecté et provoque la fin de sa
     * partie.
     */
    public synchronized void infAdvDeco(int adv) {
        threads.get(adv).advDeco();
    }

    /**
     * Informe adv que la partie est terminé.
     */
    public synchronized void informAdvGameEnd(int adv) {
        threads.get(adv).gameEnd();
    }

    // OUTILS
    /**
     * Retourne le Thread s'occupant du joueur dont le code de hashage de son
     * pseudo est code.
     */
    private synchronized ThreadListenClient getThread(int code) {
        return threads.get(code);
    }

// MAIN
    /*
     *  On lance le serveur ici. Il reste à attendre que des clients se connectent à lui
     *  et lance connectionRequest(Socket);
     */
    public static void main(String[] args) {
        Integer port;
        Integer backlog;
        String adresse;
        if (args.length == 1) {
            port = Constant.PORT_DEFAULT;
            backlog = BACKLOG_DEFAULT;
            adresse = args[0];
        } else {
            System.err.println("usage : CheckersServer ServerAddress");
            return;
        }

        ServerSocket ss = null;
        Server s = new Server();
        try {
            System.out.println(InetAddress.getLocalHost());
            ss = new ServerSocket(port, backlog, InetAddress.getByName(adresse));
            while (true) {
                try {
                    Socket skt = ss.accept();
                    Thread t = new Thread(new ThreadListenClient(skt, s));
                    t.start();
                } catch (IOException e) {
                    System.err.println("Erreur serveur");
                }
            }
        } catch (IOException e) {
            System.err.println("Le port " + +Constant.PORT_DEFAULT + " est déjà utilisé !");
        }
    }
}
