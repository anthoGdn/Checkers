package fr.gdna.server;

public class Player {

// ATTRIBUTS
    private final String name;
    private final String ip;

// CONSTRUCTEURS
    public Player(String s, String ip) {
        name = s;
        this.ip = ip;
    }

// REQUETES
    public String getName() {
        return name;
    }

    public String getip() {
        return ip;
    }

    public String toString() {
        return name;
    }

    public int hashCode() {
        // Deux joueurs ayant le même pseudo ne pourront pas être sur le même server.
        return name.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Player o = (Player) obj;
        return getName().equals(o.getName());
    }
}
